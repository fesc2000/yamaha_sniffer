/*  Copyright 2022 Felix Schmidt.

    License: GPL V3 or later

    Yamaha HTR-6030 volume and input retrieval using Teensy 4.0.

    The purpose is to obtain current volume and input settings.
    Connect R2A-CLK to pin A9 and R2A-DATA to A8. These pins are available
    at the middle connector on the top corner of the logic board.

    The signal is a simple CLK/Data bus running at ca. 1/2 MHz. CLK is active high,
    data must be sampled at rising edge. Each symbol has a length of 24 bit, followed by
    at least one idle clock.

    The content of a symbol is encoded in the lower nibble, where
    9 is the input selection (see inputs array), and
    2,3,4,5 contain volume control data (7-bit). For an exact mapping of outputs and bit positions
    see the channels array below.

    Whenever an input/volume change is detected, or when some serial character is received, last known
    data is printed as:

    :FrontL=-67   :FrontR=-67   :Center=-77   :SurroundL=-77   :SurroundR=-77   :Avg=-75  :dB=-67 :input=CD

    I did not yet find out how to exactly map the values to dB printed on the panel, but the front channels
    are a good compromise (-> taken as source for the printed dB value)

    The Teensy is meant to be powered externally so that it always knows the current setting. 
    It can't do it by it's own yet, so it relies on getting initial data when the receiver is powered on.
*/

const int clk_pin   = PIN_A9;
const int data_pin  = PIN_A8;

// the setup() method runs once, when the sketch starts

#define FIFO_LEN      0xff
#define CMD_MASK      0x0000000f        /**< Command type mask */

#define CMD_CHANNEL2  0x00000002        /**< Channel 2 */
#define CMD_CHANNEL3  0x00000003        /**< Channel 3 */
#define CMD_CHANNEL4  0x00000004        /**< Channel 4 */
#define CMD_CHANNEL5  0x00000005        /**< Channel 5 */
#define CMD_INPUT_SEL 0x00000009        /**< Input select */

#define INPUT_MASK    0x00f00000        /**< Mask of CMD_INPUT_SEL command which encodes the input */
#define INPUT_MASK2   0x00f000f0        /**< Mask of CMD_INPUT_SEL command which affects volume offset?? */

unsigned long last_irq = 0;             /**< Time of last interrupt */
unsigned long current_shift;            /**< Current shift value */
unsigned long current_val;              /**< Current symbol being worked on */
unsigned long timeout;                  /**< Timeout after which a new interrupt aborts old and starts new symbol */
unsigned long count;                    /**< Running symbol counter */
unsigned long timeouts;                 /**< Number of timeout since last good symbol */
bool          first;                    /**< true of a new symbol starts */
int           head, tail;               /**< head (isr) and tail (output) into symbol FIFO */
unsigned long values[FIFO_LEN + 1];     /**< symbol FIFO filled by ISR */
int           current_volume_in_db = 0; /**< Last known volume */
int           current_input = 0;        /**< Last known input */
unsigned long old_val[16] = { 0 };      /**< For debug. last symbol for each group */
unsigned long xored[16] = { 0 };        /**< For debug. cumulated bit changes */
int           irqs = 0;                 /**< Number of IRQs since last good symbol */
int           print_raw = 0;            /**< Whether to print raw values */
unsigned long print_raw_val = 0;        /**< Which raw value to print (0 = all) */

/* maps content of "9" symbol to input source (stored in bits 20..23).
   For each input there seems to a specific offset applied to the
   read input volume. The value in bits 4..7 also seem to play
   a role in it, so the table may contains several elements for
   a specific input, as far as i found out
*/
struct
{
  unsigned long   val;                  /**< symbol value */
  const char*     name;                 /**< input name */
  int             offset;               /**< Offset for volume calculation */
  int             adjusted_offset;      /**< user adjusted */
}
inputs[] =
{
  { 0,          "unknown",  7 }
  , {0x004f9139,  "Tuner",    4 }
  , {0x006f9139,  "CD",       7  }
  , {0x006f9199,  "CD",       4 }
  , {0x00ff4119,  "MultiCh",  7 }
  , {0x007f9139,  "DVR",      4 }
  , {0x000f9139,  "V-AUX",    4 }
  , {0x008f9139,  "DTV/CBL",  7 }
  , {0x008f9199,  "DTV/CBL",  1 }
  , {0x009f9139,  "DVD",      13 }
  , {0x005f9139,  "MD/CD-R",  13 }
  , {0, NULL, 0 }
};

#define FRONT_L 0
#define FRONT_R 1
#define CENTER  2
#define SURR_L  3
#define SURR_R  4

/* output channels (subwoofer still missing)
 */
struct
{
  unsigned long   id;             /**< Channel ID */
  int             shift;          /**< value shift */
  const char*     name;           /**< channel name */

  int             volume;         /**< Current Volume */
  int             prev_volume;    /**< Previous Volume */
}
channels[] =
{
    { 3, 17, "FrontL" }
  , { 2, 17, "FrontR" }
  , { 4, 16, "Center" }
  , { 5, 7,  "SurroundL" }
  , { 5, 16, "SurroundR" }
  , { 0, 0,  NULL }
};


/** ISR services rising clock edge */
void isr()
{
  int data          = digitalRead(data_pin);
  unsigned long tm  = micros();
  unsigned long td;

  irqs++;

  if (tm > last_irq)
    td = tm - last_irq;
  else
    td = (0xffffffffL - last_irq) + tm + 1;

  if (first)
  {
    current_val   = 0L;
    current_shift = 1L << 23;
  }
  else if ((current_shift != 0L) && (td > timeout))
  {
    current_val   = 0L;
    current_shift = 1L << 23;
    timeouts++;
  }

  if (HIGH == data)
  {
    current_val |= current_shift;
  }

  current_shift = current_shift >> 1;

  if (0 == current_shift)
  {
    /* just assume this never overflows .. */
    values[head++ & FIFO_LEN] = current_val;
    count++;
    first = true;
    irqs = 0;
  }
  else
  {
    first = false;
  }

  last_irq = tm;
}

void setup() {
  Serial.begin(115200);

  timeout       = 200;
  timeouts      = 0;
  first         = true;
  count         = 0;
  head = tail   = 0;

  attachInterrupt(digitalPinToInterrupt(clk_pin), isr, RISING);
  interrupts();
}

/** Decode symbol into dB value */
int decode(unsigned long val, int shift)
{
  int decoded;
  int offset = inputs[current_input].offset + inputs[current_input].adjusted_offset;

  offset = 0;

  val = val >> shift;
  decoded = val & 0x7f;

  if ((val & 0x80) == 0)
    decoded = -decoded + offset;
  else
    decoded -= 49 + offset;

  return decoded;
}

bool decode_volume(unsigned long raw)
{
  bool changed = false;

  for (int i = 0; channels[i].name != NULL; i++)
  {
    if (channels[i].id == (raw & CMD_MASK))
    {
      channels[i].prev_volume = channels[i].volume;
      channels[i].volume = decode(raw, channels[i].shift);
      if (channels[i].prev_volume != channels[i].volume)
      {
        changed = true;
      }
    }
  }
  return changed;
}

int current_vol()
{
  return channels[FRONT_R].volume;
}

/** Get input source from symbol (input into inputs array)
 * *p_input is unchanged if there is no match
*/
bool get_input(unsigned long val, int* p_input)
{
  if ((val & CMD_MASK) == CMD_INPUT_SEL)
  {
    /* try input mask + "volume offset" */
    for (int i = 0; inputs[i].name != NULL; i++)
    {
      if ((val & INPUT_MASK2) == (inputs[i].val & INPUT_MASK2))
      {
        *p_input = i;
        return true;
      }
    }

    /* try input mask only */
    for (int i = 0; inputs[i].name != NULL; i++)
    {
      if ((val & INPUT_MASK) == (inputs[i].val & INPUT_MASK))
      {
        *p_input = i;
        return true;
      }
    }
  }

  return false;
}

void loop()
{
  bool show_values = false;
  bool changed = false;
  int input;
  int c;

  while (tail != head)
  {
    unsigned long val = values[tail & FIFO_LEN];
    unsigned long cmd = val & CMD_MASK;

    if (print_raw &&
        ((print_raw_val == 0) ||
         (print_raw_val == cmd)))
    {
      unsigned long oval = old_val[cmd];
      xored[cmd] |= oval ^ val;
      Serial.printf("(raw 0x%08x 0x%08x 0x%08x) ", val, oval ^ val, xored[cmd]);
      Serial.println("");
      old_val[cmd] = val;
    }

    switch (cmd)
    {
      case CMD_INPUT_SEL:
        if (get_input(val, &input))
        {
          if (input != current_input)
          {
            show_values = true;
            current_input = input;
          }
        }
        break;

      case CMD_CHANNEL2:
      case CMD_CHANNEL3:
      case CMD_CHANNEL4:
      case CMD_CHANNEL5:
        changed |= decode_volume(val);
        break;
    }

    tail++;
  }

  if (changed)
  {
//    if (current_vol() != current_volume_in_db)
    {
      current_volume_in_db = current_vol();
      show_values = true;
    }
  }

  while (Serial.available() > 0)
  {
    switch (c = Serial.read())
    {
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
        print_raw = 1;
        print_raw_val = c - '0';
        xored[print_raw_val] = 0;
        Serial.printf("Showing raw for %d", print_raw_val);
        Serial.println("");
        break;

      case 'R':
        print_raw = 1;
        print_raw_val = 0;
        Serial.println("Raw mode on");
        memset ((char*)xored, 0, sizeof(xored));
        break;

      case 'r':
        print_raw = 0;
        Serial.println("Raw mode off");
        break;

      case '+':
        inputs[current_input].adjusted_offset++;
        Serial.printf("Offset is %d + %d", inputs[current_input].offset, inputs[current_input].adjusted_offset);
        Serial.println("");
        show_values = 1;
        break;

      case '-':
        inputs[current_input].adjusted_offset--;
        Serial.printf("Offset is %d + %d", inputs[current_input].offset, inputs[current_input].adjusted_offset);
        Serial.println("");
        show_values = 1;
        break;

      case '=':
        inputs[current_input].adjusted_offset = 0;
        Serial.printf("Offset is %d + %d", inputs[current_input].offset, inputs[current_input].adjusted_offset);
        Serial.println("");
        show_values = 1;
        break;

      case 'h':
        Serial.println("r : Raw mode off");
        Serial.println("R : Raw mode on");
        Serial.println("+ : Increase volume offset for current input");
        Serial.println("- : Decrease volume offset for current input");
        break;

      default:
        show_values = true;
        break;
    }
  }

  if (show_values)
  {
    int i;
    float avg = 0.0;

    for (i = 0; channels[i].name != NULL; i++)
    {
      Serial.printf(":%s=%-4d  ", channels[i].name, channels[i].volume);
      avg += (float)channels[i].volume;
    }
    Serial.printf(":%s=%d  ", "Avg", (int)(avg/(float)i));
    
    Serial.printf(":dB=%d :input=%s", current_volume_in_db, inputs[current_input].name);
    Serial.println("");
  }

#if 0
  if (timeouts)
  {
    Serial.printf("TO:%d IRQs:%d", timeouts, irqs);
    Serial.println("");
    timeouts = 0;
    irqs = 0;
  }
#endif
}
